# Attendance google meet.

Requisitos:

 - Tener una cuenta de google suite.
 - El meet creado debe ser parte de una google classroom.

Steps:

- Create a new Google Sheet.
- From within your new sheet, select the menu item Tools > Script editor. If you are presented with a welcome screen, click Blank Project.
- Click on the "Untitled project" project name, and rename to "Attendance."
Click Resources > Advanced Google Services.
In the dialog that appears, find and switch-on for the Google Classroom API, Admin Reports API and Gmail API.
